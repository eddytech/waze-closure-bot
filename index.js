require('dotenv').config()

const cron = require('node-cron')
const async = require('async')
const fastify = require('fastify')({ logger: false })

const validate = require('./model/validator')
const query = require('./model/query')
const processor = require('./model/processor')

fastify.get('/', async (req, res) => {
  res.header('content-type', 'text/html')
  return res.send(`<html><body></body><script>window.location.href = 'waze://?ll=${req.query.ll}'</script></html>`)
})

fastify.listen({port: 4040}, err => {
  if (err) {
    return console.error(err.toString())
  }

  console.log(`Application running. As scheduler : `, process.env.AS_SCHEDULER || true)

  if (process.env.AS_SCHEDULER === 'false') {
    begin()
  } else {
    // Every 5 minute
    cron.schedule('*/5 * * * *', begin, { timezone: 'Asia/Kuala_Lumpur' })
  }
})

const { boxDB, filterClosureDBPath, filterCommentDBPath } = validate()

const begin = () => {
  async.eachSeries(boxDB, (box, callback) => {
    query(box.lat_n, box.lat_s, box.lon_n, box.lon_s)
      .then(response => processor(response.data, filterClosureDBPath, filterCommentDBPath, box))
      .then(() => callback())
      .catch(err => {
        console.error(err.toString())
        callback()
      })
  }, () => {})
}
