/**
 * Run this file to get the chat_id from telegram.
 * Make sure after run this script, run telegram-set-hook.js also
 * Copy the http path first before run that.
 * Once hook is set. navigate to ngrok inspector (default localhost:4040)
 * and send a message from telegram to grab the chat_id
 *
 */

require('dotenv').config()

const os = require('os')
const http = require('http')
const path = require('path')
const ngrok = require('ngrok')
const axios = require('axios')

const requestListener = function (req, res) {
	res.writeHead(200)
	res.end('Hello, World!')
}

const server = http.createServer(requestListener)
server.listen(3000);

(async function () {
	try {
		const url = await ngrok.connect({
			addr: 3000,
			region: 'ap',
			configPath: path.join(os.homedir(), '.ngrok2', 'ngrok.yml')
		})

		await axios.post(`https://api.telegram.org/bot${process.env.TELEGRAM_MY}/setWebhook`, {url})

		console.log('Success setting the telegram webhook')
		console.log('Open ngrok inspector (Default localhost:4040) to see the request')
		console.log('Then send the message to grab the chat_id')

	} catch (err) {
		console.error('Error setting telegram token. Please try again')
	}
})()
