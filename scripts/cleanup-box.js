const path = require('path')
const jsonfile = require('jsonfile')

const boxPath = path.join(__dirname, '..', 'database', 'box.json')
const box = jsonfile.readFileSync(boxPath)
jsonfile.writeFileSync(boxPath, box, {spaces: 2})