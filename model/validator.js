const path = require('path')
const fs = require('fs')
const jsonfile = require('jsonfile')

const dbPath = path.join(__dirname, '..', 'database', 'box.json')
const filterClosureDBPath = path.join(__dirname, '..', 'database', 'd.json')
const filterCommentDBPath = path.join(__dirname, '..', 'database', 'dc.json')

module.exports = () => {

  if (!fs.existsSync(dbPath)) {
    throw new Error('Please ensure that box.json is define in database/box.json')
  }

  // If filter file not exist. create a new one
  if (!fs.existsSync(filterClosureDBPath)) {
    jsonfile.writeFileSync(filterClosureDBPath, [])
  }

  if (!fs.existsSync(filterCommentDBPath)) {
    jsonfile.writeFileSync(filterCommentDBPath, [])
  }

  const boxDB = jsonfile.readFileSync(dbPath)

  return {boxDB, filterClosureDBPath, filterCommentDBPath}
}
