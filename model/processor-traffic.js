const async = require('async')
const dayjs = require('dayjs')
const _ = require('lodash')
const jsonfile = require('jsonfile')

const { newClosure } = require('./message')
const telegram = require('./telegram')
const writeDump = require('./write-dump')

module.exports = (filterPath, jams, alerts) => new Promise((resolve, reject) => {
  let filterData = []
  try {
    filterData = jsonfile.readFileSync(filterPath)
  } catch (err) {

  }

  async.each(jams, (jam, callback) => {
    if (jam.country !== process.env.COUNTRY_CODE) {
      // no in the same country
      return callback()
    }

    if (!/^ROAD_CLOSED/.test(jam.blockType)) {
      // Not a closure
      return callback()
    }
    
    if (filterData.indexOf(jam.uuid) !== -1) {
      // Already filtered
      return callback()
    }
    
    //Jam blockstartime in dump file is 0: remove it for avoid script not working
    //if (dayjs(jam.blockStartTime).startOf('day').diff(dayjs().startOf('day'), 'day') !== 0) {
      // Not today. Probably old data
    //  return callback()
    //}

    // Try to get the alert related to this traffic report
    let alert = {}
    _.each(alerts, j => {
      const regex = new RegExp(_.escapeRegExp(jam.blockingAlertID))
      if (regex.test(j.id)) {
        alert = j
      }
    })

    if (jam.blockExpiration !== 0) {
      // EpailXi findings
      // if either 2 of this value is not 0, then consider it as wme.
      return callback()
    }

    // save to filtered data, to prevent display back again
    filterData.push(jam.uuid)

    // Construct the message
    const message = newClosure(jam, alert)

    writeDump(jam, alert)

    telegram(message)
      .then(() => callback())
      .catch(() => callback())
  }, () => {
    jsonfile.writeFileSync(filterPath, filterData)
    resolve()
  })
})
