const _ = require('lodash')
const dayjs = require('dayjs')

module.exports.newClosure = (jam, alert) => {
  const direction = jam.segments[0].isForward ? '(A-B)' : '(B-A)'
  const lon = jam.line[0].x
  const lat = jam.line[0].y

  let rt = `Unknown - ${jam.roadType}`
  if (jam.roadType === 1) {
    rt = 'Street'
  } else if (jam.roadType === 2) {
    rt = 'Primary Street'
  } else if (jam.roadType === 3) {
    rt = 'Freeway'
  } else if (jam.roadType === 4) {
    rt = 'Ramp'
  } else if (jam.roadType === 5) {
    rt = 'Minor Highway'
  } else if (jam.roadType === 6) {
    rt = 'Major Highway'
  } else if (jam.roadType === 7) {
    rt = 'Major Highway'
  } else if (jam.roadType === 8) {
    rt = 'Dirt road'
  } else if (jam.roadType === 17) {
    rt = 'Private Street'
  } else if (jam.roadType === 20) {
    rt = 'Parking Lot Road'
  } else if (jam.roadType === 22) {
    rt = 'Narrow Street'
  }

  return `
*⛔️ New app closure ${direction}*   
   
${rt} segment | Severity: ${jam.severity} | Level: ${jam.level}   
${!_.isEmpty(jam.street) ? `${jam.street}, ` : ''}${jam.city || '--NO CITY--'}   
Description : ${!_.isEmpty(_.trim(jam.blockDescription)) ? jam.blockDescription : '-'}  
${!_.isEmpty(alert) ? `Reported by : ${alert.reportBy}` : ''}

[Live Map](https://www.waze.com/livemap?zoom=17&lon=${lon}&lat=${lat}&overlay=false&cta=false) | [WME](https://www.waze.com/editor/?env=row&lon=${lon}&lat=${lat}&zoomLevel=18&segments=${jam.segments[0].ID}) | [App](https://waze.dontpushpush.com?ll=${lat},${lon})
`
}

module.exports.newComment = (alert, comment) => {
  const lon = alert.location.x
  const lat = alert.location.y

  return `
New comment by [${comment.reportBy || 'anonymous'}](https://www.waze.com/user/editor/${comment.reportBy || ''}) on a closure by [${alert.reportBy}](https://www.waze.com/user/editor/${alert.reportBy})
       
📝 ${comment.text}   
    
${!_.isEmpty(_.trim(alert.street)) ? `${alert.street}, ` : ''}${alert.city || 'Unknown City'}    
${alert.reportDescription || ''}    
[Live Map](https://www.waze.com/livemap?zoom=17&lon=${lon}&lat=${lat}&overlay=false&cta=false) | [WME](https://www.waze.com/editor/?env=row&lon=${lon}&lat=${lat}&zoomLevel=18) | [App](https://waze.dontpushpush.com?ll=${lat},${lon})
`
}
