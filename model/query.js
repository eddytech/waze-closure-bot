const axios = require('axios')

module.exports = async (top, bottom, left, right) => {
  return axios.get('https://www.waze.com/row-rtserver/web/TGeoRSS', {
    params: {
      top,
      bottom,
      left,
      right,
      types: 'traffic,alerts',
      ma: 600,
      mj: 200,
      mu: 400
    }
  })
}
