const _ = require('lodash')

const checkLargeResults = require('./check-large-results')
const processorTraffic = require('./processor-traffic')
const processorAlerts = require('./processor-alerts')

module.exports = async (results, filterClosureDBPath, filterCommentDBPath, box) => {

  if (!_.has(results, 'jams') && !_.has(results, 'alerts')) {
    // Nothing to be process...
    return null
  }

  checkLargeResults(box, results.jams || [], results.alerts || [])

  const promiseProcessorTraffic = processorTraffic(filterClosureDBPath, results.jams || [], results.alerts || [])
  const promiseProcessorAlerts = processorAlerts(filterCommentDBPath, results.alerts || [])

  await promiseProcessorTraffic
  await promiseProcessorAlerts

  return null
}
