const axios = require('axios')

module.exports = message => {
  if (process.env.SENT_TO_TELEGRAM === 'true') {
    return axios.post(`https://api.telegram.org/bot${process.env.TELEGRAM_TOKEN}/sendMessage`, {
      chat_id: process.env.CHAT_ID,
      text: escape(message),
      parse_mode: 'Markdown',
      disable_web_page_preview: true
    })
  } else {
    return Promise.resolve()
  }
}

function escape (text) {
  /* eslint-disable-next-line */
	return text.replace(/_/g, '\_').replace(/\*/g, '\*').replace(/`/g, '\`')
}
