const async = require('async')
const _ = require('lodash')
const dayjs = require('dayjs')
const jsonfile = require('jsonfile')

const { newComment } = require('./message')
const telegram = require('./telegram')
const writeDump = require('./write-dump')

module.exports = (filterPath, alerts) => new Promise((resolve, reject) => {
  let filterData = []
  try {
    filterData = jsonfile.readFileSync(filterPath)
  } catch (err) {

  }

  async.each(alerts, (alert, callback) => {
    if (alert.country !== process.env.COUNTRY_CODE) {
      // no in the same country
      return callback()
    }

    if (!/^ROAD_CLOSED/.test(alert.type)) {
      // Not a closure
      return callback()
    }

    async.each(alert.comments || [], (comment, callback2) => {
      if (_.isEmpty(_.trim(comment.text))) {
        // No message
        return callback2()
      }

      if (filterData.indexOf(comment.reportMillis) !== -1) {
        // Duplicate
        return callback2()
      }

      if (dayjs(comment.reportMillis).startOf('day').diff(dayjs().startOf('day'), 'day') !== 0) {
        // Not today. Probably old data
        return callback2()
      }

      // save to filtered data, to prevent display back again
      filterData.push(comment.reportMillis)

      // Construct the message
      comment.reportMillisDate = dayjs(comment.reportMillis).format('DD-MM-YYYY hh:mm a')
      const message = newComment(alert, comment)

      writeDump(null, comment)

      telegram(message)
        .then(() => callback2())
        .catch(() => callback2())
    }, () => callback())
  }, () => {
    jsonfile.writeFileSync(filterPath, filterData)

    resolve()
  })
})
