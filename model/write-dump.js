const path = require('path')
const jsonfile = require('jsonfile')
const _ = require('lodash')

module.exports = (jam, alert) => {
	if (process.env.DUMP_JSON === 'true') {

		if (!_.isEmpty(jam)) {
			jsonfile.writeFileSync(path.join(__dirname, '..', 'dump', `j_${jam.uuid}.json`), jam, {spaces: 2})
		}

		if (!_.isEmpty(alert)) {
			const name = _.has(alert, 'reportMillis')? alert.reportMillis : alert.uuid
			const type = _.has(alert, 'reportMillis')? 'c' : 'a'
			jsonfile.writeFileSync(path.join(__dirname, '..', 'dump', `${type}_${name}.json`), alert, {spaces: 2})
		}
	}
}