module.exports = (box, jams, alerts) => {
	const throttle = 30
	const { lat_n: latN, lat_s: latS, lon_n: lonN, lon_s: lonS } = box

	if (jams.length > throttle) {
		console.log(`Box traffic ${latN} | ${latS} | ${lonN} | ${lonS} is having more that ${throttle} results : ${jams.length}`)
	}

	if (alerts.length > throttle) {
		console.log(`Box alerts ${latN} | ${latS} | ${lonN} | ${lonS} is having more that ${throttle} results : ${alerts.length}`)
	}
}